# Simple Programming Language (SPL) Compiler
_ _ _

**This is a source to source compiler made using [Flex](https://github.com/westes/flex) and [Bison](https://www.gnu.org/software/bison) that
compiles SPL code into C code.**
_ _ _

This compiler was created as a technical experiement rather that to fulfill a
specific purpose.

Examples programs are provided [here](#examples-of-compilation) and within the test directory.


- [Simple Programming Language (SPL) Compiler](#simple-programming-language--spl--compiler)
  * [Licence (GPLv3+)](#licence--gplv3--)
  * [Instructions](#install-instructions)
    + [Download](#download)
    + [Building from source](#building-from-source)
    + [Running](#running)
  * [The source](#the-source)
- [Simple Programming Language (SPL) Description](#simple-programming-language--spl--description)
  * [Data Types](#data-types)
  * [Type system](#type-system)
  * [Examples of Compilation](#examples-of-compilation)
  * [Grammar](#grammar)

## Licence ![GPLv3+](https://www.gnu.org/graphics/gplv3-88x31.png)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Instructions
### Download
The compiler can be downloaded here:
- [spl](/uploads/641d86efbf3299e34e0073f5b482812a/spl)

Alternatively the compiler can be checked out and [built from source.](#building-from-source)
### Building from source
#### Dependencies
This project depends on [Flex](https://github.com/westes/flex) and [Bison](https://www.gnu.org/software/bison)
In the example below GCC has been used however other compilers should work.
#### Building
```
flex spl.l
bison -t spl.y
gcc -o spl.exe spl.c spl.tab.c -lfl
```
### Running
```
./spl < *Path to spl source file* | tee *file name*.c
```
After this, \*file name*.c will contain the C source code.
## The source
There are three files that are necessary to compile the compiler. These are:
- spl.l – The flex lexer file.
- spl.y – The Bison parser file.
- spl.c – The main entry point of the compiler.

# Simple Programming Language (SPL) Description
## Data Types
There are three data types that are supported:
### Characters
Characters are always represented using single quotes.
For example 'x'.
### Integers
Can be positive or negative.
Examples:
- 2 ✓
- 24 ✓
- -2 ✓
- q ✗

### Reals
Can be positive or negative.
Reals have a decimal point and always have at least one digit after the decimal
point.
Examples:
- 2.9 ✓
- 2.0 ✓
- 2.09 ✓
- \-2.0 ✓
- 2 ✗
- 2\. ✗

## Type system
SPL is a static, weakly typed language.

Below is a table showing the resulting type after performing an arithmetic
operation between two variables.

|               | **Character** | **Integer** | **Real** |
|---------------|---------------|-------------|----------|
| **Character** | Character     | Integer     | Real     |
| **Integer**   | integer       | Integer     | Real     |
| **Real**      | Real          | Real        | Real     |

## Examples of Compilation

Some examples of SPL programs that have been compiled into C code using this
compiler are provided to help understand the SPL language syntax.
The Backus–Naur form (BNF) notation for the SPL language can also be found [here.](#grammar)

<table>
  <tbody>
    <tr>
      <th><b>SPL code</b></th>
      <th><b>Resulting C code</b></th>
    </tr>
    <tr>
      <th><b>A minimal SPL program</b></th>
      <th><b>The resulting program in C</b></th>
    </tr>
<tr>
<td>
<pre>
<code>
Test1:

CODE

  NEWLINE

ENDP Test1.
</code>
</pre>
</td>
<td>
<pre>
<code lang="c">
#include &#60stdio.h>
#include &#60stdlib.h>

int main() /* Test1 */
{
/* Declerations */
printf("\n");

/* Code */
return 0;
}
</code>
</pre>
</td>
</tr>
    <tr>
      <th><b>An example of variables in SPL</b></th>
      <th><b>The resulting program in C</b></th>
    </tr>
<tr>
<td>
<pre>
<code>
Test:

DECLARATIONS
   
	a,b,c OF TYPE INTEGER;
	d,e OF TYPE REAL;
	f OF TYPE CHARACTER;
	g OF TYPE CHARACTER;

CODE

  1 -> a;
  a -> b;
  a -> d;
  1.0 -> d;
  d -> e;
  'x' -> g;
  g -> f;
  f -> d;
  d -> a;
  f -> a;

  NEWLINE

ENDP 

Test.
</code>
</pre>
</td>
<td>
<pre>
<code lang="c">
#include &#60stdio.h>
#include &#60stdlib.h>

int main() /* Test */
{
/* Declerations */
int  _a, _b, _c;
float  _d, _e;
char  _f;
char  _g;

/* Code */
_a = 1;
_b = _a;
_d = _a;
_d = 1;
_e = _d;
_g = 120;
_f = _g;
_d = _f;
_a = _d;
_a = _f;
printf("\n");
return 0;
}
</code>
</pre>
</td>
</tr>
    <tr>
      <th><b>An example of write statements in SPL</b></th>
      <th><b>The resulting program in C</b></th>
    </tr>
<tr>
<td>
<pre>
<code>
Test:

DECLARATIONS
   
	a OF TYPE INTEGER;
	d OF TYPE REAL;
	f OF TYPE CHARACTER;
	
CODE

  WRITE(0);
  NEWLINE;

  WRITE(1);
  NEWLINE;

  WRITE(3.14159);
  NEWLINE;

  WRITE(-3.14159);
  NEWLINE;

  WRITE('x');
  NEWLINE;

  WRITE('X');
  NEWLINE;

  WRITE(3, 14159, 'x');
  NEWLINE;

  1 -> a;
  3.14159 -> d;
  'x' -> f;

  WRITE(a);
  NEWLINE;

  WRITE(d);
  NEWLINE;

  WRITE(f);
  NEWLINE;

  WRITE(1, a);
  NEWLINE;

  WRITE(1, f);
  NEWLINE;

  WRITE((1));
  NEWLINE;

  WRITE((a));
  NEWLINE

ENDP 

Test.
</code></pre></td>
<td><pre><code lang="c">
#include &#60stdio.h>
#include &#60stdlib.h>

int main() /* Test */
{
/* Declerations */
int  _a;
float  _d;
char  _f;

/* Code */
printf("%i", 0);
printf("\n");
printf("%i", 1);
printf("\n");
printf("%f", 3.14159);
printf("\n");
printf("%f", -3.14159);
printf("\n");
printf("%c", 'x');
printf("\n");
printf("%c", 'X');
printf("\n");
printf("%i", 3);
printf("%i", 14159);
printf("%c", 'x');
printf("\n");
_a = 1;
_d = 3.14159;
_f = 120;
printf("%i", _a);
printf("\n");
printf("%g", _d);
printf("\n");
printf("%c", _f);
printf("\n");
printf("%i", 1);
printf("%i", _a);
printf("\n");
printf("%i", 1);
printf("%c", _f);
printf("\n");
printf("%i", 1);
printf("\n");
printf("%i", (_a));
printf("\n");
return 0;
}
</code>
</pre>
</td>
</tr>
    <tr>
      <th><b>An example of arithmetic in SPL</b></th>
      <th><b>The resulting program in C</b></th>
    </tr>
<tr>
<td>
<pre>
<code>
Test:

DECLARATIONS
   
	a,b,c OF TYPE INTEGER;
	d,e OF TYPE REAL;
	f,g OF TYPE CHARACTER;
	
CODE

  1 + 1 -> a;
  WRITE(a);
  NEWLINE;

  2 * -2 -> a;
  WRITE(a);
  NEWLINE;

  WRITE((1 - 1));
  NEWLINE;

  1 / 20 -> a;
  WRITE(a);
  NEWLINE;

  5 + 4 - 3 * 2 / 1 -> a;
  WRITE(a);
  NEWLINE;

  (5 - 4) + ((3 * 2) / 1) -> a;
  WRITE(a);
  NEWLINE;

  3.2 + 1.3 -> d;
  WRITE(d);
  NEWLINE;

  10 * 2.41 -> d;
  WRITE(d);
  NEWLINE

ENDP 

Test.
</code></pre></td>
<td><pre><code lang="c">
#include &#60stdio.h>
#include &#60stdlib.h>

int main() /* Test */
{
/* Declerations */
int  _a, _b, _c;
float  _d, _e;
char  _f, _g;

/* Code */
_a = 2;
printf("%i", _a);
printf("\n");
_a = -4;
printf("%i", _a);
printf("\n");
printf("%i", 0);
printf("\n");
_a = 0;
printf("%i", _a);
printf("\n");
_a = 3;
printf("%i", _a);
printf("\n");
_a = 7;
printf("%i", _a);
printf("\n");
_d = 4.5;
printf("%g", _d);
printf("\n");
_d = 24.1;
printf("%g", _d);
printf("\n");
return 0;
}
</code>
</pre>
</td>
</tr>
    <tr>
      <th><b>If statements in SPL</b></th>
      <th><b>The resulting program in C</b></th>
    </tr>
<tr>
<td>
<pre>
<code>
Test:

DECLARATIONS
   
  a, b OF TYPE INTEGER;
	
CODE

  1 -> a;
  2 -> b;
  IF 1 > 0 THEN
    WRITE(a)
  ENDIF;
  NEWLINE;

  IF 1 < 0 THEN
    WRITE(a)
  ENDIF;
  NEWLINE;

  IF 2 > a THEN
    WRITE(a)
  ENDIF;
  NEWLINE;

  IF a >= 0 THEN
    WRITE(a)
  ENDIF;
  NEWLINE;

  IF a<=b THEN
    WRITE('a')
  ELSE
    WRITE('b')
  ENDIF;
  NEWLINE;

  IF a = b THEN
    WRITE('y')
  ELSE
    WRITE('n')
  ENDIF;
  NEWLINE;

  IF a <> b THEN
    WRITE('y')
  ELSE
    WRITE('n')
  ENDIF;
  NEWLINE;

  IF NOT a = b THEN
    WRITE('y')
  ELSE
    WRITE('n')
  ENDIF;
  NEWLINE;

  IF a = a AND a = b THEN
    WRITE('y')
  ELSE
    WRITE('n')
  ENDIF;
  NEWLINE;

  IF a = a OR a = b THEN
    WRITE('y')
  ELSE
    WRITE('n')
  ENDIF;
  NEWLINE

ENDP 

Test.
</code>
</pre>
</td>
<td>
<pre>
<code lang="c">
#include &#60stdio.h>
#include &#60stdlib.h>

int main() /* Test */
{
/* Declerations */
int  _a, _b;

/* Code */
_a = 1;
_b = 2;
if ((1 > 0))
{
printf("%i", _a);

}
printf("\n");
if ((1 < 0))
{
printf("%i", _a);

}
printf("\n");
if ((2 > _a))
{
printf("%i", _a);

}
printf("\n");
if ((_a >= 0))
{
printf("%i", _a);

}
printf("\n");
if ((_a <= _b))
{
printf("%c", 'a');

} else {
printf("%c", 'b');

}
printf("\n");
if ((_a == _b))
{
printf("%c", 'y');

} else {
printf("%c", 'n');

}
printf("\n");
if ((_a != _b))
{
printf("%c", 'y');

} else {
printf("%c", 'n');

}
printf("\n");
if (!((_a == _b)))
{
printf("%c", 'y');

} else {
printf("%c", 'n');

}
printf("\n");
if ((_a == _a) && (_a == _b))
{
printf("%c", 'y');

} else {
printf("%c", 'n');

}
printf("\n");
if ((_a == _a) || (_a == _b))
{
printf("%c", 'y');

} else {
printf("%c", 'n');

}
printf("\n");
return 0;
}
</code>
</pre>
</td>
</tr>
    <tr>
      <th><b>An example of for loops in SPL</b></th>
      <th><b>The resulting program in C</b></th>
    </tr>
<tr>
<td>
<pre>
<code>
Test:

DECLARATIONS
   
  a OF TYPE INTEGER;
	
CODE

  FOR a IS 1 BY 1 TO 5 DO
     WRITE(a)
  ENDFOR;
  NEWLINE

ENDP 

Test.
</code>
</pre>
</td>
<td>
<pre>
<code lang="c">
#include &#60stdio.h>
#include &#60stdlib.h>

int main() /* Test */
{
/* Declerations */
int  _a;

/* Code */
register int _;
for(_a = 1; _ = _a, (_a - 5) * ((_ > 0) - (_ < 0)) <= 0;_a += 1)
{
printf("%i", _a);
}
printf("\n");
return 0;
}
</code>
</pre>
</td>
</tr>
    <tr>
      <th><b>An example of read statements in SPL</b></th>
      <th><b>The resulting program in C</b></th>
    </tr>
<tr>
<td>
<pre>
<code>
Test:

DECLARATIONS
   
  a OF TYPE INTEGER;
  b OF TYPE REAL;
  c OF TYPE CHARACTER;
	
CODE

  READ(a);
  WRITE(a);
  NEWLINE;
  READ(b);
  WRITE(b);
  NEWLINE;
  READ(c);
  WRITE(c);
  NEWLINE

ENDP 

Test.
</code>
</pre>
</td>
<td>
<pre>
<code lang="c">
#include &#60stdio.h>
#include &#60stdlib.h>

int main() /* Test */
{
/* Declerations */
int  _a;
float  _b;
char  _c;

/* Code */
scanf(" %i", &_a);
printf("%i", _a);
printf("\n");
scanf(" %g", &_b);
printf("%g", _b);
printf("\n");
scanf(" %c", &_c);
printf("%c", _c);
printf("\n");
return 0;
}
</code>
</pre>
</td>
</tr>
</tbody>
</table>

## Grammar

The Grammar of the SPl language has been defined below using Backus–Naur form (BNF) notation.

``<program ::= <identifier> ":" <block> ENDP <identifier> "."``

``<block> ::= DECLARATIONS <declaration_block> CODE <statement_list> |
			CODE <statement_list>``

``<declaration_block> ::= <identifier_list> OF TYPE <type> ";" |
						<identifier_list> OF TYPE <type> ";" <declaration_block>``

``<type> ::= CHARACTER | INTEGER | REAL``

``<statement_list> ::= <statement> |
						<statement> ";" <statement_list>``

``<statement> ::= <assignment_statement> |
				<if_statement> |
				<do_statement> |
				<while_statement> |
				<for_statement> |
				<write_statement> |
				<read_statement>``

``<assignment_statement> ::= <expression> "->" <identifier>``

``<if_statement> ::= IF <conditional> THEN <statement_list> ENDIF |
					IF <conditional> THEN <statement_list> ELSE <statement_list> ENDIF``

``<do_statement> ::= DO <statement_list> WHILE <conditional> ENDDO``

``<while_statement> ::= WHILE <conditional> DO <statement_list> ENDWHILE``

``<for_statement> ::= FOR <identifier> IS <expression> BY <expression> TO <expression> DO <statement_list> ENDFOR``

``<write_statement> ::= WRITE "(" <output_list> ")" |
						NEWLINE``

``<read_statement> ::= READ "(" <identifier> ")"``

``<output_list> ::= <value> |
					<value> "," <output_list>``

``<conditional> ::= <condition> |
					<condition> AND <conditional> |
					<condition> OR <conditional>``

``<condition> ::= <expression> <comparator> <expression> |
				NOT <condition>``

``<comparator> ::= "=" | "<>" | "<" | ">" | "<=" | ">="``

``<expression> ::= <term> |
					<term> "+" <expression> |
					<term> "-" <expression>``

``<term> ::= <value> |
			<value> "*" <expression> |
			<value> "/" <expression>``

``<value> ::= <identifier> |
			<constant> |
			"(" <expression> ")"``

``<constant> ::= <number_constant> |
				<character_constant>``

``<character_constant> ::= "'" <character> "'"``

``<number_constant> ::= <integer> | <real>``

``<integer> ::= <number> | "-" <number>``

``<real> ::= <number> "." <number> | "-" <number> "." <number> ``

``<number> ::= <digit> |<digit> <number>``

``<identifier_list> ::= <identifier> |
						<identifier> "," <identifier_list>``

``<identifier> ::= <character>
				| <identifier> <character>
				| <identifier> <digit>``

``<character> ::= "A"|"B"|"C"|"D"|"E"|"F"|"G"|"H"|"I"|"J"|"K"|"L"|"M"|"N"|"O"|"P"|"Q"|"R"|"S"|"T"|"U"|"V"|"W"|"X"|"Y"|"Z"|
				"a"|"b"|"c"|"d"|"e"|"f"|"g"|"h"|"i"|"j"|"k"|"l"|"m"|"n"|"o"|"p"|"q"|"r"|"s"|"t"|"u"|"v"|"w"|"x"|"y"|"z"``

``<digit> ::= "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"``