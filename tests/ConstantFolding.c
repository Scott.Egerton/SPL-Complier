/*
Copyright 2016, 2017 Scott Egerton

This file is part of SPL-Compiler.

    SPL-Compiler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPL-Compiler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPL-Compiler.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>

int main() /* ProgV */
{
/* Declerations */
int  _integer, _i;

/* Code */
scanf(" %i", &_integer);
if ((_integer <= 9) || (_integer >= 10))
{
printf("%i", _integer);

}
printf("%f", 133.1);
printf("\n");
printf("%f", 39);
printf("\n");
printf("%f", 34.1);
printf("\n");
printf("%i", 18);
printf("\n");
printf("%f", 1020.1);
printf("\n");
printf("\n");
printf("%i", 97);
printf("\n");
printf("%i", 98);
printf("\n");
printf("%i", 99);
printf("\n");
printf("%i", 122);
printf("\n");
printf("%i", 100);
printf("\n");
printf("%i", 65);
printf("\n");
printf("%f", 67.1);
printf("%c", 'l');
printf("%i", 5);
printf("\n");
_i = -0.255102;
return 0;
}
