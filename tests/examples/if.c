/*
Copyright 2016, 2017 Scott Egerton

This file is part of SPL-Compiler.

    SPL-Compiler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPL-Compiler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPL-Compiler.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>

int main() /* Test */
{
/* Declerations */
int  _a, _b;

/* Code */
_a = 1;
_b = 2;
if ((1 > 0))
{
printf("%i", _a);

}
printf("\n");
if ((1 < 0))
{
printf("%i", _a);

}
printf("\n");
if ((2 > _a))
{
printf("%i", _a);

}
printf("\n");
if ((_a >= 0))
{
printf("%i", _a);

}
printf("\n");
if ((_a <= _b))
{
printf("%c", 'a');

} else {
printf("%c", 'b');

}
printf("\n");
if ((_a == _b))
{
printf("%c", 'y');

} else {
printf("%c", 'n');

}
printf("\n");
if ((_a != _b))
{
printf("%c", 'y');

} else {
printf("%c", 'n');

}
printf("\n");
if (!((_a == _b)))
{
printf("%c", 'y');

} else {
printf("%c", 'n');

}
printf("\n");
if ((_a == _a) && (_a == _b))
{
printf("%c", 'y');

} else {
printf("%c", 'n');

}
printf("\n");
if ((_a == _a) || (_a == _b))
{
printf("%c", 'y');

} else {
printf("%c", 'n');

}
printf("\n");
return 0;
}
