/*
Copyright 2016, 2017 Scott Egerton

This file is part of SPL-Compiler.

    SPL-Compiler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPL-Compiler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPL-Compiler.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>

int main() /* Test */
{
/* Declerations */
int  _a, _b, _c;
float  _d, _e;
char  _f, _g;

/* Code */
_a = 2;
printf("%i", _a);
printf("\n");
_a = -4;
printf("%i", _a);
printf("\n");
printf("%i", 0);
printf("\n");
_a = 0;
printf("%i", _a);
printf("\n");
_a = 3;
printf("%i", _a);
printf("\n");
_a = 7;
printf("%i", _a);
printf("\n");
_d = 4.5;
printf("%g", _d);
printf("\n");
_d = 24.1;
printf("%g", _d);
printf("\n");
return 0;
}
