/*
Copyright 2016, 2017 Scott Egerton

This file is part of SPL-Compiler.

    SPL-Compiler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPL-Compiler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPL-Compiler.  If not, see <http://www.gnu.org/licenses/>.
*/

%{
#ifdef PRINT
#define TOKEN(token) printf("Token: " #token "\n")
#define STRING(token) printf("Token: " #token " %s\n", yytext) //no need to know the type of anything other than identifiers which cannot be discovered from the lexer.
#define INTEGER(token) printf("Token: " #token " %d\n", atoi(yytext))
#define REAL(token) printf("Token: " #token " %g\n", atof(yytext))
#define CHAR(token) printf("Token: " #token " %c\n", yytext[1])
#else
#define	TOKEN(token) return(token)
#define	STRING(token) yylval.iVal = installId(yytext, ' '); return(token)
#define	INTEGER(token) yylval.iVal = atoi(yytext); return(token)
#define	REAL(token) yylval.iVal = installId(yytext, 'f'); return(token)
#define CHAR(token) yylval.iVal = yytext[1]; return(token)


/* Declare Symbol Table Type and Array as imported types */

#include <string.h>
extern SYMTABNODEPTR symTab[SYMTABSIZE];
extern int currentSymTabSize;
int installId(char *, char);
#endif
%}

delim				[ \t\n\r]
ws				{delim}+
digit				[0-9]
character			[A-Za-z]
identifier			{character}[0-9A-Za-z]*
integer				{digit}+
real				{integer}\.{integer}
character_constant		'{character}'

%%

{ws}				; /* Do Nothing */
"+"				TOKEN(PLUS);
"*"				TOKEN(TIMES);
"-"				TOKEN(MINUS);
"/"				TOKEN(DIVIDE);

"("				TOKEN(LEFT_BRACKET);
")"				TOKEN(RIGHT_BRACKET);

"="				TOKEN(EQUALS);
"<>"				TOKEN(NOT_EQUALS);
"<"				TOKEN(LESS_THAN);
">"				TOKEN(GREATER_THAN);
"<="				TOKEN(LESS_THAN_EQUALS);
">="				TOKEN(GREATER_THAN_EQUALS);
"->"				TOKEN(ASSIGNMENT);

","				TOKEN(COMMA);
"."				TOKEN(DOT);

":"				TOKEN(COLON);
";"				TOKEN(SEMICOLON);

CHARACTER			TOKEN(CHARACTER);
INTEGER				TOKEN(INTEGER); //TODO: Is this needed?
REAL				TOKEN(REAL); //TODO: IS this needed?
OF				TOKEN(OF);
TYPE				TOKEN(TYPE);

DECLARATIONS			TOKEN(DECLARATIONS);
CODE				TOKEN(CODE);
ENDP				TOKEN(ENDP);

IF				TOKEN(IF);
THEN				TOKEN(THEN);
ELSE				TOKEN(ELSE);
ENDIF				TOKEN(ENDIF);

AND				TOKEN(AND);
OR				TOKEN(OR);
NOT				TOKEN(NOT);

DO				TOKEN(DO);
WHILE				TOKEN(WHILE);
ENDDO				TOKEN(ENDDO);
ENDWHILE			TOKEN(ENDWHILE);
FOR				TOKEN(FOR);
IS				TOKEN(IS);
BY				TOKEN(BY);
TO				TOKEN(TO);
ENDFOR				TOKEN(ENDFOR);

READ				TOKEN(READ);
WRITE				TOKEN(WRITE);
NEWLINE				TOKEN(NEWLINE);

{identifier} STRING(IDENTIFIER);

{integer} INTEGER(INTEGER);

{real} REAL(REAL);

{character_constant} CHAR(CHARACTER_CONSTANT);

%%
#ifndef PRINT

/* code for a simple symbol table, which is an array of pointers to
   structs, each of which contains an identifier.
*/


SYMTABNODEPTR newSymTabNode()
{
    return ((SYMTABNODEPTR)malloc(sizeof(SYMTABNODE)));
}

int lookup(char *s)
{
    extern SYMTABNODEPTR symTab[SYMTABSIZE];
    extern int currentSymTabSize;
    int i;

    for(i=0; i<currentSymTabSize; i++)
    {
        if(strncmp(s,symTab[i]->identifier,IDLENGTH) == 0)
        {
            return (i);
        }
    }
    return (-1);    
}

/* Look up an identifier in the symbol table, if its there return
   its index.  If its not there, put it in the end position,
   as long as the table isn't full, and return its index.
*/

int installId(char *id, char type) 
{
	extern SYMTABNODEPTR symTab[SYMTABSIZE]; 
    extern int currentSymTabSize;
    int index;

    index = lookup(id);
    if (index >= 0)
    {
        return (index);
    }
    else 
       if (currentSymTabSize >= SYMTABSIZE) 
          /* SYMTAB is full */
          return (NOTHING) ;
    else
    {
       symTab[currentSymTabSize] = newSymTabNode();
       /* Recommended code for preventing buffer overrun on bounded strings */
       strncpy(symTab[currentSymTabSize]->identifier,id,IDLENGTH);
       symTab[currentSymTabSize]->identifier[IDLENGTH-1] = '\0'; // adds null terminator
	   symTab[currentSymTabSize]->type = type;
	   symTab[currentSymTabSize]->assigned = 0; //false
       return(currentSymTabSize++);
    }
}
#endif
