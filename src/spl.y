/*
Copyright 2008, Peter Parsons, BCT
Copyright 2016, 2017 Scott Egerton

This file is part of SPL-Compiler.

    SPL-Compiler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SPL-Compiler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SPL-Compiler.  If not, see <http://www.gnu.org/licenses/>.
*/

%{
/* declare some standard headers to be used to import declarations
   and libraries into the parser. */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <limits.h>

/* make forward declarations to avoid compiler warnings */
int yylex ();
int yyerror ();


/* 
   Some constants.
*/

  /* These constants are used later in the code */
#define SYMTABSIZE     500
#define IDLENGTH       150
#define NOTHING        -1
#define INDENTOFFSET    2

	enum ParseTreeNodeType { PROGRAM, BLOCK, DECLARATION_BLOCK, VAR_TYPE, STATEMENT_LIST, STATEMENT, ASSIGNMENT_STATEMENT, IF_STATEMENT,
							DO_STATEMENT, WHILE_STATEMENT, FOR_STATEMENT, FOR_EXPRESSION, WRITE_STATEMENT, READ_STATEMENT, OUTPUT_LIST, CONDITIONAL,
							CONDITION, COMPARATOR, EXPRESSION, TERM, VALUE, CONSTANT, NUMBER_CONSTANT, INTEGER_VAL, REAL_VAL, MINUS_REAL_VAL, IDENTIFIER_LIST
							};  
                          /* Add more types here, as more nodes
                                           added to tree */
										   
	char *NodeName[] = {"PROGRAM", "BLOCK", "DECLARATION_BLOCK", "VAR_TYPE", "STATEMENT_LIST", "STATEMENT", "ASSIGNMENT_STATEMENT", "IF_STATEMENT",
						"DO_STATEMENT", "WHILE_STATEMENT", "FOR_STATEMENT", "FOR_EXPRESSION", "WRITE_STATEMENT", "READ_STATEMENT", "OUTPUT_LIST", "CONDITIONAL",
						"CONDITION", "COMPARATOR", "EXPRESSION", "TERM", "VALUE", "CONSTANT", "NUMBER_CONSTANT", "INTEGER_VAL", "REAL_VAL", "MINUS_REAL_VAL", "IDENTIFIER_LIST"
	};  
#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef NULL
#define NULL 0
#endif

/* ------------- parse tree definition --------------------------- */

struct treeNode {
    int  item;
    int  nodeIdentifier;
    struct treeNode *first;
    struct treeNode *second;
    struct treeNode *third;
  };

typedef  struct treeNode TREE_NODE;

typedef  TREE_NODE        *TERNARY_TREE;

/* ------------- forward declarations --------------------------- */

TERNARY_TREE create_node(int,int,TERNARY_TREE,TERNARY_TREE,TERNARY_TREE);
void PrintCode(TERNARY_TREE);
void PrintTree(TERNARY_TREE, int);
void PrintIdentifier(int);
char ExpressionType(TERNARY_TREE);

void ConstantFolding(TERNARY_TREE);
int intEvaluateExpression(TERNARY_TREE, char*);
float floatEvaluateExpression(TERNARY_TREE, char*);

/* ------------- symbol table definition --------------------------- */

struct symTabNode {
    char identifier[IDLENGTH];
	char type;
	char assigned;
};

typedef  struct symTabNode SYMTABNODE;
typedef  SYMTABNODE        *SYMTABNODEPTR;

SYMTABNODEPTR  symTab[SYMTABSIZE]; 

int currentSymTabSize = 0;
char current_type = ' ';
int forCounter = 0; // needed to prevent redecleration of _ variable when printing for statements

%}

/****************/
/* Start symbol */
/****************/

%start  program

/**********************/
/* Action value types */
/**********************/

%union {
    int iVal;
    TERNARY_TREE  tVal;
}

/* We can declare types of tree nodes */

/* These are the types of lexical tokens -> iVal */
%token<iVal> IDENTIFIER CHARACTER_CONSTANT INTEGER REAL 
/* Some tokens do not return a value */
%token COLON ENDP DOT OF TYPE SEMICOLON CHARACTER ASSIGNMENT IF THEN ELSE ENDIF DO WHILE 
%token ENDDO ENDWHILE FOR IS BY TO ENDFOR WRITE LEFT_BRACKET RIGHT_BRACKET NEWLINE READ COMMA AND OR 
%token NOT EQUALS NOT_EQUALS LESS_THAN GREATER_THAN LESS_THAN_EQUALS GREATER_THAN_EQUALS
%token PLUS MINUS TIMES DIVIDE DECLARATIONS CODE 

/* Whereas Rules return a tVal type (Tree) */
%type<tVal> program block declaration_block var_type statement_list statement assignment_statement if_statement
%type<tVal> do_statement while_statement for_statement for_expression write_statement read_statement output_list conditional
%type<tVal> condition comparator expression term value constant number_constant integer_val real_val identifier_list

%%
/* ----------------------- grammar rules --------------------------- */

program : IDENTIFIER COLON block ENDP IDENTIFIER DOT
			{ TERNARY_TREE ParseTree;
				if($1 == $5)
				{
					ParseTree = create_node($1, PROGRAM, $3, NULL, NULL);
					//#if YYDEBUG == 0
					#if DEBUG == TRUE
						PrintTree(ParseTree, 0);
					#else
						PrintCode(ParseTree);
					#endif
					//#endif
				}
				else
					yyerror("WARNING: Program identifiers are not the same");
			$$ = ParseTree;
			};

block : DECLARATIONS declaration_block CODE statement_list
		 {
			$$ = create_node(NOTHING, BLOCK, $2, $4, NULL);
		 }
		| CODE statement_list
		 {
			$$ = create_node(NOTHING, BLOCK, $2, NULL, NULL);
		 }

declaration_block : identifier_list OF TYPE var_type SEMICOLON
					{
						$$ = create_node(NOTHING, DECLARATION_BLOCK, $1, $4, NULL);
					}
					| identifier_list OF TYPE var_type SEMICOLON declaration_block
					{
						$$ = create_node(NOTHING, DECLARATION_BLOCK, $1, $4, $6);
					}

var_type : CHARACTER
		{
			$$ = create_node(CHARACTER, VAR_TYPE, NULL, NULL, NULL);
		}
		| INTEGER
		{
			$$ = create_node(INTEGER, VAR_TYPE, NULL, NULL, NULL);
		}
		| REAL
		{
			$$ = create_node(REAL, VAR_TYPE, NULL, NULL, NULL);
		}

statement_list : statement
				{
					$$ = create_node(NOTHING, STATEMENT_LIST, $1, NULL, NULL);
				}
				| statement SEMICOLON statement_list
				{
					$$ = create_node(NOTHING, STATEMENT_LIST, $1, $3, NULL);
				}

statement : assignment_statement
			{
				$$ = create_node(NOTHING, STATEMENT, $1, NULL, NULL);
			}
			| if_statement
			{
				$$ = create_node(NOTHING, STATEMENT, $1, NULL, NULL);
			}
			| do_statement
			{
				$$ = create_node(NOTHING, STATEMENT, $1, NULL, NULL);
			}
			| while_statement
			{
				$$ = create_node(NOTHING, STATEMENT, $1, NULL, NULL);
			}
			| for_statement
			{
				$$ = create_node(NOTHING, STATEMENT, $1, NULL, NULL);
			}
			| write_statement
			{
				$$ = create_node(NOTHING, STATEMENT, $1, NULL, NULL);
			}
			| read_statement
			{
				$$ = create_node(NOTHING, STATEMENT, $1, NULL, NULL);
			}

assignment_statement : expression ASSIGNMENT IDENTIFIER
						{
							$$ = create_node($3, ASSIGNMENT_STATEMENT, $1, NULL, NULL);
						}

if_statement : IF conditional THEN statement_list ENDIF
				{
					$$ = create_node(NOTHING, IF_STATEMENT, $2, $4, NULL);
				}
				| IF conditional THEN statement_list ELSE statement_list ENDIF
				{
					$$ = create_node(NOTHING, IF_STATEMENT, $2, $4, $6);
				}

do_statement : DO statement_list WHILE conditional ENDDO
				{
					$$ = create_node(NOTHING, DO_STATEMENT, $2, $4, NULL);
				}

while_statement : WHILE conditional DO statement_list ENDWHILE
					{
						$$ = create_node(NOTHING, WHILE_STATEMENT, $2, $4, NULL);
					}

for_statement : for_expression statement_list ENDFOR
				{
					$$ = create_node(NOTHING, FOR_STATEMENT, $1, $2, NULL);
				}

for_expression : FOR IDENTIFIER IS expression BY expression TO expression DO
				{
					$$ = create_node($2, FOR_EXPRESSION, $4, $6, $8);
				}


write_statement : WRITE LEFT_BRACKET output_list RIGHT_BRACKET
				{
					$$ = create_node(NOTHING, WRITE_STATEMENT, $3, NULL, NULL);
				}
				| NEWLINE
				{
					$$ = create_node(NOTHING, WRITE_STATEMENT, NULL, NULL, NULL);
				}

read_statement : READ LEFT_BRACKET IDENTIFIER RIGHT_BRACKET
				{
					$$ = create_node($3, READ_STATEMENT, NULL, NULL, NULL);
				}

output_list : value
				{
					$$ = create_node(NOTHING, OUTPUT_LIST, $1, NULL, NULL);
				}
				| value COMMA output_list
				{
					$$ = create_node(NOTHING, OUTPUT_LIST, $1, $3, NULL);
				}

conditional : condition
				{
					$$ = create_node(NOTHING, CONDITIONAL, $1, NULL, NULL);
				}
				| condition AND conditional
				{
					$$ = create_node(AND, CONDITIONAL, $1, $3, NULL);
				}
				| condition OR conditional
				{
					$$ = create_node(OR, CONDITIONAL, $1, $3, NULL);
				}

condition : expression comparator expression
			{
				$$ = create_node(NOTHING, CONDITION, $1, $2, $3);
			}
			| NOT condition
			{
				$$ = create_node(NOTHING, CONDITION, $2, NULL, NULL);
			}
					
comparator : EQUALS
			{
				$$ = create_node(EQUALS, COMPARATOR, NULL, NULL, NULL);
			}
			| NOT_EQUALS
			{
				$$ = create_node(NOT_EQUALS, COMPARATOR, NULL, NULL, NULL);
			}
			| LESS_THAN
			{
				$$ = create_node(LESS_THAN, COMPARATOR, NULL, NULL, NULL);
			}
			| GREATER_THAN
			{
				$$ = create_node(GREATER_THAN, COMPARATOR, NULL, NULL, NULL);
			}
			| LESS_THAN_EQUALS
			{
				$$ = create_node(LESS_THAN_EQUALS, COMPARATOR, NULL, NULL, NULL);
			}
			| GREATER_THAN_EQUALS
			{
				$$ = create_node(GREATER_THAN_EQUALS, COMPARATOR, NULL, NULL, NULL);
			}
			

expression : term
			{
				$$ = create_node(NOTHING, EXPRESSION, $1, NULL, NULL);
			}
			| expression PLUS term
			{
				$$ = create_node(PLUS, EXPRESSION, $1, $3, NULL);
			}
			| expression MINUS term
			{
				$$ = create_node(MINUS, EXPRESSION, $1, $3, NULL);
			}

term : 	value
		{
			$$ = create_node(NOTHING, TERM, $1, NULL, NULL);
		}
		| term TIMES value
		{
			$$ = create_node(TIMES, TERM, $1, $3, NULL);
		}
		| term DIVIDE value
		{
			$$ = create_node(DIVIDE, TERM, $1, $3, NULL);
		}

value : IDENTIFIER
		{
			$$ = create_node($1, VALUE, NULL, NULL, NULL);
		}
		| constant
		{
			$$ = create_node(NOTHING, VALUE, $1, NULL, NULL);
		}
		| LEFT_BRACKET expression RIGHT_BRACKET
		{
			$$ = create_node(LEFT_BRACKET, VALUE, $2, NULL, NULL);
		}

constant : number_constant
			{
				$$ = create_node(NOTHING, CONSTANT, $1, NULL, NULL);
			}
			| CHARACTER_CONSTANT
			{
				$$ = create_node($1, CONSTANT, NULL, NULL, NULL);
			}

number_constant : integer_val
			{
				$$ = create_node(NOTHING, NUMBER_CONSTANT, $1, NULL, NULL);
			}
			| real_val
			{
				$$ = create_node(NOTHING, NUMBER_CONSTANT, $1, NULL, NULL);
			}

integer_val : INTEGER
			{
				$$ = create_node($1, INTEGER_VAL, NULL, NULL, NULL);
			}
			| MINUS INTEGER
			{
				$$ = create_node(0-$2, INTEGER_VAL, NULL, NULL, NULL);
			}

real_val : REAL
		{
			$$ = create_node($1, REAL_VAL, NULL, NULL, NULL);
		}
		| MINUS REAL
		{
			$$ = create_node($2, MINUS_REAL_VAL, NULL, NULL, NULL);
		}

identifier_list : IDENTIFIER
				{
					$$ = create_node($1, IDENTIFIER_LIST, NULL, NULL, NULL);
				}
				| IDENTIFIER COMMA identifier_list
				{
					$$ = create_node($1, IDENTIFIER_LIST, $3, NULL, NULL);
				}
%%

void PrintTree(TERNARY_TREE t, int tabs)
{
	if (NULL == t)
		return;
	
	for(int i = 0; i < tabs; i++)
		printf("	"); //indent properly
	
	if (NOTHING != t->item) {
		switch (t->nodeIdentifier) {
		case CONSTANT:
			if (NULL == t->first) //character constant else fall through
				printf("CONSTANT: '%c'\n", t->item);
			break;
		case INTEGER_VAL:
			printf("INTEGER: %i\n", t->item);
			break;
		case REAL_VAL:
			printf("REAL: %s\n", symTab[t->item]->identifier);
			return;
		case MINUS_REAL_VAL:
			printf("REAL: -%s\n", symTab[t->item]->identifier);
			return;
		case VAR_TYPE:
			if (CHARACTER == t->item)
				printf("VAR_TYPE: char\n");
			else if (INTEGER == t->item)
				printf("VAR_TYPE: int\n");
			else if (REAL == t->item)
				printf("VAR_TYPE: float\n");
			break;
		case EXPRESSION:
			printf("EXPRESSION: ");
			if (NULL != t->second) {
				if (t->item == PLUS)
					printf(" +\n");
				else if (MINUS == t->item)
					printf(" -\n");
				else
				{
					yyerror("ERROR: Unknown Operator in expression");
					printf("ERROR UNKNOWN OPERATOR\n");
				}
			}
			break;
		case TERM:
			printf("TERM: ");
			if (NULL != t->second) {
				if (t->item == TIMES)
					printf(" *\n");
				else if (DIVIDE == t->item)
					printf(" /\n");
				else
				{
					yyerror("ERROR: Unknown Operator in term");
					printf("ERROR UNKNOWN OPERATOR\n");
				}
			}
			break;
		case CONDITIONAL:
			if (NULL != t->second) {
				if (AND == t->item)
					printf("CONDITIONAL: &&\n");
				else if (OR == t->item)
					printf("CONDITIONAL: ||\n");
				else
					yyerror("ERROR: Unknown Operator in conditional");
			}
			break;
		case CONDITION:
			if (NULL == t->second) //not condition
				printf("CONDITION: !");
			break;
		case COMPARATOR:
			switch (t->item) {
			case EQUALS:
				printf("%s =\n", NodeName[t->nodeIdentifier]);
				break;
			case NOT_EQUALS:
				printf("%s !=\n", NodeName[t->nodeIdentifier]);
				break;
			case GREATER_THAN:
				printf("%s >\n", NodeName[t->nodeIdentifier]);
				break;
			case LESS_THAN:
				printf("%s <\n", NodeName[t->nodeIdentifier]);
				break;
			case LESS_THAN_EQUALS:
				printf("%s <=\n", NodeName[t->nodeIdentifier]);
				break;
			case GREATER_THAN_EQUALS:
				printf("%s >=\n", NodeName[t->nodeIdentifier]);
				break;
			default:
				yyerror("ERROR: Unknown Comparator: %s", NodeName[t->nodeIdentifier]);
				printf("%s Error unknown comparator\n", NodeName[t->nodeIdentifier]);
				break;
			}
			break;
		case VALUE:
			if (NULL != t->first) {//constant / LEFT_BRACKET expression RIGHT_BRACKET
				if (LEFT_BRACKET == t->item) //LEFT_BRACKET expression RIGHT_BRACKET
				{
					printf("VALUE\n");
					break;
				}
			}
			// else fall through to default
		default:
			if (0 > t->nodeIdentifier || sizeof(NodeName) <= t->nodeIdentifier) //Within the array bounds
				printf("Unknown nodeIdentifier: %i\n", t->nodeIdentifier);
			else
				printf("%s %s\n", NodeName[t->nodeIdentifier], symTab[t->item]->identifier);
		}
	} else {
		if (0 > t->nodeIdentifier || sizeof(NodeName) <= t->nodeIdentifier) //Within the array bounds
			 printf("Unknown nodeIdentifier: %i\n", t->nodeIdentifier);
		else
			printf("%s\n", NodeName[t->nodeIdentifier]);
	}
	PrintTree(t->first, tabs++);
	PrintTree(t->second, tabs++);
	PrintTree(t->third, tabs++);
}

void PrintCode(TERNARY_TREE t)
{
	if (NULL == t) return;
	
	if (0 > t->nodeIdentifier || sizeof(symTab) <= t->nodeIdentifier) //Within the array bounds
	{
		yyerror("Error: NodeIdentifier outside of symbol table bounds");
		return;
	}
		
	switch (t->nodeIdentifier)
	{
		case PROGRAM:
			printf("#include <stdio.h>\n#include <stdlib.h>\n\nint main() /* %s */\n{\n", symTab[t->item]->identifier);
			PrintCode(t->first);
			printf("return 0;\n}\n");
			break;
		case BLOCK:
			printf("/* Declerations */\n");
			PrintCode(t->first);
			printf("\n/* Code */\n");
			PrintCode(t->second);
			break;
		case DECLARATION_BLOCK:
			PrintCode(t->second);
			printf(" ");
			if (CHARACTER == t->second->item)
				current_type = 'c';
			else if (INTEGER == t->second->item)
				current_type = 'i';
			else if (REAL == t->second->item)
				current_type = 'g';
			PrintCode(t->first);
			printf(";\n");
			PrintCode(t->third);
			break;
		case VAR_TYPE:
			if (CHARACTER == t->item)
				printf("char ");
			else if (INTEGER == t->item)
				printf("int ");
			else if (REAL == t->item)
				printf("float ");
			break;
		case STATEMENT_LIST:
			PrintCode(t->first);
			if (NULL != t->second)
				PrintCode(t->second);
			break;
		case STATEMENT:
			PrintCode(t->first);
			break;
		case ASSIGNMENT_STATEMENT:
			PrintIdentifier(t->item);
			printf(" = ");
			ConstantFolding(t->first);
			symTab[t->item]->assigned = TRUE; // used in error checking for unassigned variables
			printf(";\n");
			break;
		case IF_STATEMENT:
			printf("if (");
			PrintCode(t->first);
			printf(")\n{\n");
			PrintCode(t->second);
			printf("\n}");
			if (NULL != t->third) {
				printf(" else {\n");
				PrintCode(t->third);
				printf("\n}");
			}
			printf("\n");
			break;
		case DO_STATEMENT:
			printf("do {\n");
			PrintCode(t->first);
			printf("} while(");
			PrintCode(t->second);
			printf(");\n");
			break;
		case WHILE_STATEMENT:
			printf("while(");
			PrintCode(t->first);
			printf(") {\n");
			PrintCode(t->second);
			printf("}\n");
			break;
		case FOR_STATEMENT:
			printf("register int _"); // called _ as it is a valid var name in c but not SPL therefore there is no risk of redecleration
			for (int i = 0; i < forCounter; i++)
				printf("_");
			printf(";\n");
			forCounter++;
			
			PrintCode(t->first);
			PrintCode(t->second);
			printf("}\n");
			break;
		case FOR_EXPRESSION:
			printf("for(");
			PrintIdentifier(t->item);
			printf(" = ");
			ConstantFolding(t->first);
			printf("; ");

			for (int i = 0; i < forCounter; i++)
				printf("_");
			printf(" = ");
			PrintIdentifier(t->item);
			printf(", (");
			PrintIdentifier(t->item);
			printf(" - ");
			ConstantFolding(t->third);
			printf(") * ((");
			for (int i = 0; i < forCounter; i++)
				printf("_");
			printf(" > 0) - (");
			for (int i = 0; i < forCounter; i++)
				printf("_");
			printf(" < 0)) <= 0;");

			PrintIdentifier(t->item);
			printf(" += ");
			ConstantFolding(t->second);
			printf(")\n{\n");
			break;
		case OUTPUT_LIST:
			current_type = ExpressionType(t->first);
			printf("printf(\"%%%c\", ", current_type); //print variables e.g. printf("%d", x);
			PrintCode(t->first);
			printf(");\n");
			if (NULL != t->second)
				PrintCode(t->second);
			break;
		case WRITE_STATEMENT:
			if (NULL != t->first)
				PrintCode(t->first);
			else
				printf("printf(\"\\n\");\n"); // newline statement
			break;
		case READ_STATEMENT:
			printf("scanf(\" \%%%c\", &", symTab[t->item]->type);
			PrintIdentifier(t->item);
			printf(");\n");
			symTab[t->item]->assigned = TRUE;
			break;
		case CONDITIONAL:
			PrintCode(t->first);
			if (NULL != t->second) {
				if (AND == t->item)
					printf(" && ");
				else if (OR == t->item)
					printf(" || ");
				else
				{
					yyerror("ERROR: Unknown Operator in conditional");
					exit(0);
				}
				PrintCode(t->second);
			}
			break;
		case CONDITION:
			if (NULL == t->second) { //not condition
				printf("!(");
				PrintCode(t->first);
				printf(")");
			} else { // other condition e.g. 2 > 1
				printf("(");
				ConstantFolding(t->first);
				PrintCode(t->second);
				ConstantFolding(t->third);
				printf(")");
			}
			break;
		case COMPARATOR:
			switch (t->item) {
			case EQUALS:
				printf(" == ");
				break;
			case NOT_EQUALS:
				printf(" != ");
				break;
			case GREATER_THAN:
				printf(" > ");
				break;
			case LESS_THAN:
				printf(" < ");
				break;
			case LESS_THAN_EQUALS:
				printf(" <= ");
				break;
			case GREATER_THAN_EQUALS:
				printf(" >= ");
				break;
			default:
				yyerror("ERROR: Unknown comparator");
				exit(0);
				break;
			}
			break;
		case EXPRESSION:
			PrintCode(t->first);
			if (NULL != t->second) { // term +/- expression
				if (t->item == PLUS)
					printf(" + ");
				else if  (t->item == MINUS)
					printf(" - ");
				else
				{
					yyerror("ERROR: Unknown Operator in expression");
					exit(0);
				}
				PrintCode(t->second);
			}
			break;
		case TERM:
			PrintCode(t->first);
			if (NULL != t->second) {
				if (t->item == TIMES)
					printf(" * ");
				else if (DIVIDE == t->item)
					printf(" / ");
				else
				{
					yyerror("ERROR: Unknown Operator in term");
					exit(0);
				}
				PrintCode(t->second);
			}
			break;
		case VALUE:
			if (NULL != t->first) {//constant / LEFT_BRACKET expression RIGHT_BRACKET
				if (LEFT_BRACKET == t->item) //LEFT_BRACKET expression RIGHT_BRACKET
					ConstantFolding(t);
				 else //constant
					PrintCode(t->first);
			}
			else //identifier
				PrintIdentifier(t->item);
			break;
		case CONSTANT:
			if (NULL != t->first) //number constant
				PrintCode(t->first);
			else //character constant
				printf("'%c'", t->item);
			break;
		case NUMBER_CONSTANT:
			PrintCode(t->first);
			break;
		case INTEGER_VAL:
			printf("%i", t->item);
			break;
		case REAL_VAL:
			printf("%s", symTab[t->item]->identifier);
			break;
		case MINUS_REAL_VAL:
			printf("-%s", symTab[t->item]->identifier);
			break;
		case IDENTIFIER_LIST:
			symTab[t->item]->type = current_type; // all identifiers in an identifier list are the same type
			PrintIdentifier(t->item);
			if (NULL != t->first) { // multiple identifiers
				printf(", ");
				PrintCode(t->first);
			}
			break;
		default:
			PrintCode(t->first);
			PrintCode(t->second);
			PrintCode(t->third);
			break;
	}
}

void PrintIdentifier(int item)
{
	if (' ' == symTab[item]->type) //not yet declared
	{
		yyerror("ERROR: Undeclared variable used");
		exit(0);
	}
	printf("_%s", symTab[item]->identifier); // avoids issues with variables having the same name as keywords
}

//Evaluate the type of an expression
char ExpressionType(TERNARY_TREE expression)
{
	if (LEFT_BRACKET == expression->item) // expression enclosed in brackets
		return ExpressionType(expression->first);
	
	else if (CONSTANT == expression->nodeIdentifier) //constant
		return (NOTHING != expression->item) ? 'c' : ExpressionType(expression->first); // 'c'  for character constant, recurse for number constant

	else if (NUMBER_CONSTANT == expression->nodeIdentifier) //number constant
		return (INTEGER_VAL == expression->first->nodeIdentifier) ? 'i' : 'f'; // 'i'  for integer_val, 'f'  for real_val
	
	else if (VALUE == expression->nodeIdentifier) // identifier or constant
		return (NOTHING == expression->item) ? ExpressionType(expression->first): symTab[expression->item]->type; // return type for identifiers, recurse for constants
	
	else if (TERM == expression->nodeIdentifier || EXPRESSION == expression->nodeIdentifier) //recurse deeper for terms and expressions
	{
		char firstType = ExpressionType(expression->first);
		if (NULL == expression->second) return firstType; // no arthimetic operations, just a constant
		char secondType = ExpressionType(expression->second);
		if ('f' == firstType || 'f' == secondType) // cast to float if either is of type float
			return 'f';
		else if ('i' == firstType || 'i' == secondType) // if no floats, cast to int if either is of type int
			return 'i';
		else
			return 'c';
	}
	else
	{
		yyerror("ERROR: Expression has unknown type");
		printf("%s %s\n", NodeName[expression->nodeIdentifier], symTab[expression->item]->identifier);
		return -1;
	}
}

// Constant folding
void ConstantFolding(TERNARY_TREE expression)
{
	char error = FALSE;
	char type = ExpressionType(expression);
	if ('f' == type)
	{
		float floatValue = floatEvaluateExpression(expression, &error);
		if (FALSE == error) // no errors
			printf("%g", floatValue);
	}
	else if (-1 != type) // error
	{
		int intValue = intEvaluateExpression(expression, &error);
		if (FALSE == error) // no errors
			printf("%d", intValue);
	}
	else
		error = TRUE;
	
	if (TRUE == error) // if there's a problem just print without constant folding
	{
		if (LEFT_BRACKET == expression->item) //LEFT_BRACKET expression RIGHT_BRACKET
		{
			printf("(");
			PrintCode(expression->first);
			printf(")");
		}
	else
		PrintCode(expression);
	}
}

// Constant folding
int intEvaluateExpression(TERNARY_TREE expression, char* error)
{
	if (LEFT_BRACKET == expression->item) // expression enclosed in brackets
		return intEvaluateExpression(expression->first, error);
	
	else if (CONSTANT == expression->nodeIdentifier) //constant
		return (NOTHING != expression->item) ? expression->item : intEvaluateExpression(expression->first, error);

	else if (NUMBER_CONSTANT == expression->nodeIdentifier) //number constant
		return expression->first->item;
	
	else if (VALUE == expression->nodeIdentifier) // identifier or constant
	{
		if (NOTHING == expression->item) // constant
			return intEvaluateExpression(expression->first, error);
		else // identifier
		{
			*error = TRUE;
			return -1;
		}
	}
	else if (TERM == expression->nodeIdentifier || EXPRESSION == expression->nodeIdentifier) //go deeper for terms and expressions
	{
		if (NULL == expression->second) return intEvaluateExpression(expression->first, error); // no arthimetic operations, just a constant
		else {
			switch (expression->item)
			{
				case PLUS:
					return intEvaluateExpression(expression->first, error) + intEvaluateExpression(expression->second, error);
					break;
				case MINUS:
					return intEvaluateExpression(expression->first, error) - intEvaluateExpression(expression->second, error);
					break;
				case TIMES:
					return intEvaluateExpression(expression->first, error) * intEvaluateExpression(expression->second, error);
					break;
				case DIVIDE:
					return intEvaluateExpression(expression->first, error) / intEvaluateExpression(expression->second, error);
					break;
			}
			expression->item = NOTHING;
			expression->second = NULL;
		}
	}
	else
	{
		yyerror("ERROR: Unknown node found during constant folding optimisation: %s", expression->nodeIdentifier);
	}
}

// Constant folding
float floatEvaluateExpression(TERNARY_TREE expression, char* error)
{
	if (LEFT_BRACKET == expression->item) // expression enclosed in brackets
		return floatEvaluateExpression(expression->first, error);
	
	else if (CONSTANT == expression->nodeIdentifier) //constant
		return (NOTHING != expression->item) ? expression->item : floatEvaluateExpression(expression->first, error);

	else if (NUMBER_CONSTANT == expression->nodeIdentifier) //number constant
	{
		if (REAL_VAL == expression->first->nodeIdentifier)
			return atof(symTab[expression->first->item]->identifier);
		else if  (MINUS_REAL_VAL == expression->first->nodeIdentifier)
			return (0.0-atof(symTab[expression->first->item]->identifier));
		else if (INTEGER_VAL == expression->first->nodeIdentifier)
			return expression->first->item;
		else
			yyerror("ERROR: Number constant has unknown identifier: %s", expression->nodeIdentifier);
	}
	else if (VALUE == expression->nodeIdentifier) // identifier or constant
	{
		if (NOTHING == expression->item) //constant
			return floatEvaluateExpression(expression->first, error);
		else // identifier
		{
			*error = TRUE;
			return -1.0;
		}
	}
	else if (TERM == expression->nodeIdentifier || EXPRESSION == expression->nodeIdentifier) //go deeper for terms and expressions
	{
		if (NULL == expression->second) return floatEvaluateExpression(expression->first, error); // no arthimetic operations, just a constant
		else {
			switch (expression->item)
			{
				case PLUS:
					return floatEvaluateExpression(expression->first, error) + floatEvaluateExpression(expression->second, error);
					break;
				case MINUS:
					return floatEvaluateExpression(expression->first, error) - floatEvaluateExpression(expression->second, error);
					break;
				case TIMES:
					return floatEvaluateExpression(expression->first, error) * floatEvaluateExpression(expression->second, error);
					break;
				case DIVIDE:
					return floatEvaluateExpression(expression->first, error) / floatEvaluateExpression(expression->second, error);
					break;
			}
			expression->item = NOTHING;
			expression->second = NULL;
		}
	}
	else
	{
		yyerror("ERROR: Unknown node found during constant folding optimisation: %s", expression->nodeIdentifier);
	}
}

/* Code for routines for managing the Parse Tree */

TERNARY_TREE create_node(int ival, int case_identifier, TERNARY_TREE p1,
			 TERNARY_TREE  p2, TERNARY_TREE  p3)
{
	TERNARY_TREE t;
	t = (TERNARY_TREE)malloc(sizeof(TREE_NODE));
    t->item = ival;
    t->nodeIdentifier = case_identifier;
    t->first = p1;
    t->second = p2;
    t->third = p3;
    return (t);
}

/* Put other auxiliary functions here */

#include "lex.yy.c"
